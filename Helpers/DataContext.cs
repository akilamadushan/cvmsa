using cvms.Models.Security;
using Microsoft.EntityFrameworkCore;
 
namespace cvms.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
 
        public DbSet<AppIdentityUser> Users { get; set; }
    }
}