using AutoMapper;
using cvms.Dtos;
using cvms.Models.Security;

namespace cvms.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AppIdentityUser, UserDto>();
            CreateMap<UserDto, AppIdentityUser>();
        }
    }
}