using Microsoft.AspNetCore.Identity;

namespace cvms.Models.Security
{
    public class AppIdentityUser:IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}