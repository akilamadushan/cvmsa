//using System.Data.Entity;
//using cvms.Persistence;
//using System.Data.Entity;
//using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using cvms.Models.Security;

namespace cvms.Models
{
    public class AppIdentityDbContext 
      : IdentityDbContext<AppIdentityUser, AppIdentityRole, string>
    {
        public DbSet<Recruitment> Recruitments { get; set; }
        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Interview> Interviews { get; set; }
        public DbSet<Reference> References { get; set; }

        

        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<InterviewCandidate>().HasKey(ic => 
            new { ic.CandidateId, ic.InterviewId} );
        }
    
        // protected override void OnModelCreating(DbModelBuilder modelBuilder)
        // {
        //     base.OnModelCreating(modelBuilder);
        //     //AspNetUsers -> User
        //     modelBuilder.Entity<ApplicationUser>()
        //         .ToTable("User");
        //     //AspNetRoles -> Role
        //     modelBuilder.Entity<IdentityRole>()
        //         .ToTable("Role");
        //     //AspNetUserRoles -> UserRole
        //     modelBuilder.Entity<IdentityUserRole>()
        //         .ToTable("UserRole");
        //     //AspNetUserClaims -> UserClaim
        //     modelBuilder.Entity<IdentityUserClaim>()
        //         .ToTable("UserClaim");
        //     //AspNetUserLogins -> UserLogin
        //     modelBuilder.Entity<IdentityUserLogin>()
        //         .ToTable("UserLogin");
        // }
    }
}