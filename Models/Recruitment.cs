using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cvms.Models
{
    [Table("Recruitments")]
    public class Recruitment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(50)]
        public string Position { get; set; }
        public double? MinimumExperience { get; set; }
        public bool Archived { get; set; }
        public bool Recruited { get; set; }
        public string Notes { get; set; }
        public ICollection<Interview> Interviews { get; set; }
        public ICollection<Candidate> Candidates { get; set; }
        public ICollection<Advertisement> Advertisements { get; set; }

        public Recruitment()
        {
            Interviews = new Collection<Interview>();
            Candidates = new Collection<Candidate>();
            Advertisements = new Collection<Advertisement>();
        }
    }
}