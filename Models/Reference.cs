using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cvms.Models
{
    [Table("References")]
    public class Reference
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string Value { get; set; }
        public Candidate Candidate { get; set; }
        public int CandidateId { get; set; }
    }
}