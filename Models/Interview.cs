using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cvms.Models
{
    [Table("Interviews")]
    public class Interview
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        //public int Index { get; set; } //index of the interview for candidate
        public DateTime Date { get; set; }
        [StringLength(50)]
        public string Status { get; set; }
        public string Remark { get; set; }
        public bool Archived { get; set; }
        public string Notes { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(100)]
        public string Interviewer { get; set; }
        public Recruitment Recruitment { get; set; }
        public int RecruitmentId { get; set; }
        //public ICollection<InterviewCandidate> Candidates { get; set; }
        
        public Interview()
        {
            //Candidates = new Collection<InterviewCandidate>();
        }
    }
}