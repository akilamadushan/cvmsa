using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cvms.Models
{
    [Table("Candidates")]
    public class Candidate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(15)]
        public string Mobile { get; set; }
        [StringLength(15)]
        public string Telephone { get; set; }
        public double? YearsOfExperience { get; set; }
        public bool MultipleApplications { get; set; }
        public DateTime? AppliedDate { get; set; }
        [StringLength(50)]
        public string AppliedPosition { get; set; }
        [StringLength(50)]
        public string Status { get; set; }
        public bool Archived { get; set; }
        public string Notes { get; set; }
        [StringLength(255)]
        public string CVPath { get; set; }
        public int RecruitmentId { get; set; }
        public ICollection<Reference> References { get; set; }//Social media links
        //public ICollection<Interview> Interviews { get; set; }
        //public ICollection<InterviewCandidate> Interviews { get; set; }

        public Candidate()
        {
            References = new Collection<Reference>();
            //Interviews = new Collection<InterviewCandidate>();
        }
    }
}