import { Recruitment } from './../models/recruitment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()

export class RecruitmentService {
  //private headers: HttpHeaders;
  //private accessPointUrl: string = '';

    constructor(private http: HttpClient) { 
    //this.headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  }

  getRecruitments() {
    return this.http.get('/api/recruitments')
        .map((res: any) => res.json());
  }

  create(recruitment: any) {
    return this.http.post('/api/recruitments', recruitment)
        .map((res: any) => res.json());
  }
}
;