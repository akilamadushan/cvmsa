import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AdvertisementService {

    constructor(private http: HttpClient) { }

  create(advertisement: any) {
    return this.http.post('/api/advertisements', advertisement)
        .map((res: any) => res.json());
  }

  getAdvertisements(){
    return this.http.get('/api/advertisements')
        .map((res: any) => res.json());
  }

}
