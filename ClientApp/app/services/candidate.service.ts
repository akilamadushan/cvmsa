import { Recruitment } from './../models/recruitment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CandidateService {

    constructor(private http: HttpClient) { }

  create(candidate: any) {
    return this.http.post('/api/candidates', candidate)
        .map((res: any) => res.json());
  }

  getCandidates() {
    return this.http.get('/api/candidates')
        .map((res: any) => res.json());
  }

  getCandidatesById() {
    return this.http.get('/api/candidates')
        .map((res: any) => res.json());
  }
}
