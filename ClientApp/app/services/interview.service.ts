import { Interview } from './../models/interview';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class InterviewService {

    constructor(private http: HttpClient) { }

  create(interview: Interview) {
    return this.http.post('/api/interviews', interview)
        .map((res: any) => res.json());
  }

  getInterviews(){
    return this.http.get('/api/interviews')
        .map((res: any) => res.json());
  }
}
