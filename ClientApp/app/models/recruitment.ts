export interface Recruitment {
    position: string;
    minimumExperience: number;
    archived: boolean;
    recruited: boolean;
    notes: string;
}