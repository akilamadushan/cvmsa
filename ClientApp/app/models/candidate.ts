export interface Candidate {
    name: string,
    email: string,
    mobile: string,
    telephone: string,
    yearsOfExperience: number,
    multipleApplications: boolean,
    appliedDate: string,
    appliedPosition: string,
    status: string,
    archived: boolean,
    notes: string,
    CVPath: string,
    recruitmentId: number
}