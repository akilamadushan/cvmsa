export interface Advertisement {
    media: string,
    from: string,
    to: string,
    cost: number,
    recruitmentId: number
}