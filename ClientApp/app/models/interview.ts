export interface Interview {
    date: string,
    status: string,
    remark: string,
    archived: boolean,
    notes: string,
    email: string,
    interviewer: string,
    recruitmentId: number
}
