import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CandidateComponent } from './components/candidate/candidate.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { AdvertisementComponent } from './components/advertisement/advertisement.component';
import { InterviewComponent } from './components/interview/interview.component';
import { RecruitmentComponent } from './components/recruitment/recruitment.component';
import { RecruitmentService } from './services/recruitment.service';
import { AdvertisementService } from './services/advertisement.service';
import { CandidateService } from './services/candidate.service';
import { InterviewService } from './services/interview.service';
import { RecruitmentListComponent } from './components/recruitment-list/recruitment-list.component';
import { InterviewListComponent } from './components/interview-list/interview-list.component';
import { CandidateListComponent } from './components/candidate-list/candidate-list.component';
import { AdvertisementListComponent } from './components/advertisement-list/advertisement-list.component';
import { AuthGuard } from './_guards/auth.guard';
import { AlertService } from './_services/alert.service';
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';
//import { JwtInterceptor } from './_helpers/jwt.interceptor';
//import { ErrorInterceptor } from './_helpers/error.interceptor';
import { AlertComponent } from './_directives/alert.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        AdvertisementComponent,
        CandidateComponent,
        DashboardComponent,
        InterviewComponent,
        RecruitmentComponent,
        AdvertisementListComponent,
        CandidateListComponent,
        InterviewListComponent,
        RecruitmentListComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: 'advertisement', component: AdvertisementComponent },
            { path: 'candidate', component: CandidateComponent },
            { path: 'dashboard', component: DashboardComponent},
            { path: 'interview', component: InterviewComponent},
            { path: 'recruitment', component: RecruitmentComponent},
            { path: 'advertisement-list', component: AdvertisementListComponent },
            { path: 'candidate-list', component: CandidateListComponent },
            { path: 'interview-list', component: InterviewListComponent },
            { path: 'recruitment-list', component: RecruitmentListComponent },

            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },

            { path: '**', redirectTo: 'dashboard' }
        ])
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        //{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        //{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        RecruitmentService,
        AdvertisementService,
        CandidateService,
        InterviewService
    ]
})
export class AppModuleShared {
}
