import { FormGroup, FormControl } from '@angular/forms';
import { Candidate } from './../../models/candidate';
import { CandidateService } from './../../services/candidate.service';
import { Component, OnInit } from '@angular/core';
import { RecruitmentService } from '../../services/recruitment.service';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent implements OnInit {
  title: any;
  candidate: Candidate = {
    name: '',
    email: '',
    mobile: '',
    telephone: '',
    yearsOfExperience: 0,
    multipleApplications: false,
    appliedDate: '',
    appliedPosition: '',
    status: '',
    archived: false,
    notes: '',
    CVPath: '',
    recruitmentId: 0
  }

  constructor(private candidateService: CandidateService,
    private recruitmentService: RecruitmentService) { }
    recruitments: any;

  ngOnInit() {
    this.title = 'Candidate';
    this.recruitmentService.getRecruitments().subscribe((recruitments: any) => {
      this.recruitments = recruitments;
      console.log('recruitments ', this.recruitments);
    });
  }

  submit() {
    this.candidateService.create(this.candidate)
        .subscribe((x: any) => console.log(x));
  }

}
