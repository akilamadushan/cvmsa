import { InterviewService } from './../../services/interview.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interview-list',
  templateUrl: './interview-list.component.html',
  styleUrls: ['./interview-list.component.css']
})
export class InterviewListComponent implements OnInit {
  interviews: any;

  constructor(private interviewService: InterviewService) { }

  ngOnInit() {
      this.interviewService.getInterviews().subscribe((interviews: any) => {
      this.interviews = interviews;
      console.log('interviews',this.interviews);
      console.log('arr interviews',Array.of(this.interviews)[0]);
    });
  }

}
