import { Interview } from './../../models/interview';
import { FormGroup, FormControl } from '@angular/forms';
import { InterviewService } from './../../services/interview.service';
import { CandidateService } from './../../services/candidate.service';
import { Component, OnInit } from '@angular/core';
import { RecruitmentService } from '../../services/recruitment.service';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.css']
})
export class InterviewComponent implements OnInit {

  title: any;
  interview: Interview = {
    date: '',
    status: '',
    remark: '',
    archived: false,
    notes: '',
    email: '',
    interviewer: '',
    recruitmentId: 1
  }

  constructor(private interviewService: InterviewService,
    private recruitmentService: RecruitmentService,
    private candidateService: CandidateService) {
  }
  recruitments: any;
  candidates: any;

  ngOnInit() {
    this.title = "Interview";
      this.recruitmentService.getRecruitments().subscribe((recruitments: any) => {
      this.recruitments = recruitments;
      console.log('recruitments ', this.recruitments);
    });
  }

  onRecruitmentChange() {
    console.log('recruitment: ',this.interview.recruitmentId);    
    this.candidateService.getCandidatesById().subscribe((candidates: any) => {
      this.candidates = candidates;
      console.log('candidates: ',this.candidates);
    });
  }

  submit() {
    this.interviewService.create(this.interview)
        .subscribe((x: any) => console.log(x));
  }

}
