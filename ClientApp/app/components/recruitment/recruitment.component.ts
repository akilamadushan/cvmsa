import { RecruitmentService } from './../../services/recruitment.service';
import { Recruitment } from './../../models/recruitment';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-recruitment',
  templateUrl: './recruitment.component.html',
  styleUrls: ['./recruitment.component.css']
})
export class RecruitmentComponent implements OnInit {

  title: any;
  formdata: any;
  recruitment: Recruitment = {
    position: '',
    minimumExperience: 0,
    archived: false,
    recruited: false,
    notes: ''
  }

  constructor(
    private recruitmentService: RecruitmentService) { }

  ngOnInit() {
    this.title = "Recruitment";
    this.formdata = new FormGroup({
      //position: new FormControl("hshd")
    });
  }

  submit() {
    this.recruitmentService.create(this.recruitment)
      .subscribe(
        (x: any) => console.log(x),
        (err: any) => {
          // this.toastService.error({
          //   title: 'Error',
          //   msg: 'An unexpected error happened',
          //   theme: 'bootstrap',
          //   showClose: true,
          //   timeout: 500000
          // });
        });
  }
}
