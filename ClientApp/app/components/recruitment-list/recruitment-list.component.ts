import { RecruitmentService } from './../../services/recruitment.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recruitment-list',
  templateUrl: './recruitment-list.component.html',
  styleUrls: ['./recruitment-list.component.css']
})
export class RecruitmentListComponent implements OnInit {
  //public recruitments: Array<any> = [];
  recruitments: any;

  constructor(private recruitmentService: RecruitmentService) {
    //recruitmentService.get().subscribe((data: any) => this.recruitments = data);
   }

  ngOnInit() {
      this.recruitmentService.getRecruitments().subscribe((recruitments: any) => this.recruitments = recruitments);
    console.log('recruitments ', this.recruitments);
  }

}
