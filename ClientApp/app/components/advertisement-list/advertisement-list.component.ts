import { Advertisement } from './../../models/advertisement';
import { AdvertisementService } from './../../services/advertisement.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-advertisement-list',
  templateUrl: './advertisement-list.component.html',
  styleUrls: ['./advertisement-list.component.css']
})
export class AdvertisementListComponent implements OnInit {
  advertisements: any;

  constructor(private advertisementService: AdvertisementService) { }

  ngOnInit() {
    this.advertisementService.getAdvertisements().subscribe((advertisements: any) => {
      this.advertisements = advertisements;
      console.log(this.advertisements);
      console.log('arr interviews',Array.of(this.advertisements));
      console.log('arr interviews',Array.of(this.advertisements)[0]);
    })
  }

}
