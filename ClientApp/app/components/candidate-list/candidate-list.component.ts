import { CandidateService } from './../../services/candidate.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.component.html',
  styleUrls: ['./candidate-list.component.css']
})
export class CandidateListComponent implements OnInit {
  candidates: any;

  constructor(private candidateService: CandidateService) { }

  ngOnInit() {
      this.candidateService.getCandidates().subscribe((candidates: any) => {
      this.candidates = candidates;
      console.log(this.candidates);
      console.log('arr interviews',Array.of(this.candidates));
      console.log('arr interviews',Array.of(this.candidates)[0]);
    })
  }

}
