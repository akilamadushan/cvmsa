import { AdvertisementService } from './../../services/advertisement.service';
import { Advertisement } from './../../models/advertisement';
import { Component, OnInit } from '@angular/core';
import { RecruitmentService } from '../../services/recruitment.service';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.css']
})
export class AdvertisementComponent implements OnInit {
  title: any;
  advertisement: Advertisement = {
    media: '',
    from: '',
    to: '',
    cost: 0,
    recruitmentId: 1
  }
  constructor(private advertisementService: AdvertisementService,
    private recruitmentService: RecruitmentService) { }

  recruitments: any;

  ngOnInit() {
    this.title = "Advertisement";
      this.recruitmentService.getRecruitments().subscribe((recruitments: any) => {
      this.recruitments = recruitments;
      console.log('recruitments ', this.recruitments);
    });
    //this.advertisement.From = Date.now().toString();
    //this.advertisement.To = Date.now().toString();
  }

  submit() {
    this.advertisementService.create(this.advertisement)
      .subscribe(x => console.log(x));
  }

}
