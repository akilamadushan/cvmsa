﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using cvms.Models;

namespace WebApplication1.Controllers
{
    [Produces("application/json")]
    [Route("api/Interviews")]
    public class InterviewsController : Controller
    {
        private readonly AppIdentityDbContext _context;

        public InterviewsController(AppIdentityDbContext context)
        {
            _context = context;
        }

        // GET: api/Interviews
        [HttpGet]
        public IEnumerable<Interview> GetInterviews()
        {
            return _context.Interviews;
        }

        // GET: api/Interviews/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInterview([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var interview = await _context.Interviews.SingleOrDefaultAsync(m => m.Id == id);

            if (interview == null)
            {
                return NotFound();
            }

            return Ok(interview);
        }

        // PUT: api/Interviews/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInterview([FromRoute] int id, [FromBody] Interview interview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != interview.Id)
            {
                return BadRequest();
            }

            _context.Entry(interview).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InterviewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Interviews
        [HttpPost]
        public async Task<IActionResult> PostInterview([FromBody] Interview interview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Interviews.Add(interview);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInterview", new { id = interview.Id }, interview);
        }

        // DELETE: api/Interviews/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInterview([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var interview = await _context.Interviews.SingleOrDefaultAsync(m => m.Id == id);
            if (interview == null)
            {
                return NotFound();
            }

            _context.Interviews.Remove(interview);
            await _context.SaveChangesAsync();

            return Ok(interview);
        }

        private bool InterviewExists(int id)
        {
            return _context.Interviews.Any(e => e.Id == id);
        }
    }
}