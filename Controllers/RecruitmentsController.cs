﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using cvms.Models;

namespace WebApplication1.Controllers
{
    //[Produces("application/json")]
    [Route("api/Recruitments")]
    public class RecruitmentsController : Controller
    {
        private readonly AppIdentityDbContext _context;

        public RecruitmentsController(AppIdentityDbContext context)
        {
            _context = context;
        }

        // GET: api/Recruitments
        [HttpGet]
        public IEnumerable<Recruitment> GetRecruitments()
        {
            return _context.Recruitments;
        }

        // GET: api/Recruitments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRecruitment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var recruitment = await _context.Recruitments.SingleOrDefaultAsync(m => m.Id == id);

            if (recruitment == null)
            {
                return NotFound();
            }

            return Ok(recruitment);
        }

        // PUT: api/Recruitments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRecruitment([FromRoute] int id, [FromBody] Recruitment recruitment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != recruitment.Id)
            {
                return BadRequest();
            }

            _context.Entry(recruitment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecruitmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Recruitments
        [HttpPost]
        public async Task<IActionResult> PostRecruitment([FromBody] Recruitment recruitment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Recruitments.Add(recruitment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRecruitment", new { id = recruitment.Id }, recruitment);
        }

        // DELETE: api/Recruitments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRecruitment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var recruitment = await _context.Recruitments.SingleOrDefaultAsync(m => m.Id == id);
            if (recruitment == null)
            {
                return NotFound();
            }

            _context.Recruitments.Remove(recruitment);
            await _context.SaveChangesAsync();

            return Ok(recruitment);
        }

        private bool RecruitmentExists(int id)
        {
            return _context.Recruitments.Any(e => e.Id == id);
        }
    }
}