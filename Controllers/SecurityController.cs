using System.Threading.Tasks;
using cvms.Models;
using cvms.Models.Security;
using cvms.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace cvms.Controllers
{
    [Produces("application/json")]
    [Route("api/Security")]
    public class SecurityController : Controller
    {
        private readonly AppIdentityDbContext _context;

        private readonly UserManager<AppIdentityUser> userManager;
        private readonly SignInManager<AppIdentityUser> signInManager;

        public SecurityController(AppIdentityDbContext context)
        {
            //this.userManager = userManager;
            //this.signInManager = signInManager;
            _context = context;
        }

        //[Route("/api/User/Register")]
        [HttpPost]
        public async Task<IActionResult> PostRegister([FromBody] AppIdentityUser user)
        {
            //throw new System.Exception();
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRecruitment", new { id = user.Id }, user);
            // if (!ModelState.IsValid)
            //     return View(model);

            // var user = new AppIdentityUser
            // {
            //     UserName = model.UserName,
            //     Email = model.Email,
            //     Age = model.Age
            // };

            // var result = await this.userManager.CreateAsync(user, model.Password);
            // if (result.Succeeded)
            // {
            //     //await this.signInManager.SignInAsync(user, isPersistent: false);

            //     // var confrimationCode = 
            //     //     await this.userManager.GenerateEmailConfirmationTokenAsync(user);

            //     // var callbackurl = Url.Action(
            //     //     controller: "Security",
            //     //     action: "ConfirmEmail",
            //     //     values: new { userId = user.Id, code = confrimationCode },
            //     //     protocol: Request.Scheme);

            //     return RedirectToAction("Index", "Home");
            // }

            // return View(model);
        }

        
    }
}